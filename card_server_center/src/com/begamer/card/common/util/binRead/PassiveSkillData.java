package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class PassiveSkillData implements PropertyReader {
	
	public int index;
	
	public int number;
	
	public String name;
	
	public int type;
	
	public int numbers;
	
	public int star;
	
	public int level;
	
	public int exp;
	
	public int action;
	
	public String SE;
	
	public float SETime;
	
	public String icon;
	
	public String describe;
	
	public int sell;
	
	private static HashMap<Integer, PassiveSkillData> data = new HashMap<Integer, PassiveSkillData>();
	private static List<PassiveSkillData> dataList=new ArrayList<PassiveSkillData>();
	
	@Override
	public void addData()
	{
		data.put(index, this);
		dataList.add(this);
	}
	
	@Override
	public void resetData()
	{

		data.clear();
	}
	
	@Override
	public void parse(String[] ss)
	{

	}
	
	public static PassiveSkillData getData(int passiveSkillId)
	{

		return data.get(passiveSkillId);
	}
	
	/** 所有被动技能* */
	public static List<PassiveSkillData> getAllDatas()
	{
		return dataList;
	}
}
