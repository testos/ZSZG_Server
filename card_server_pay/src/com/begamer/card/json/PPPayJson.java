package com.begamer.card.json;


public class PPPayJson
{
	public String order_id;
	public String billno;
	public String account;
	public String amount;
	public int status;
	public String app_id;
	public String uuid;
	public String zone;
	public String roleid;
	
	public String getOrder_id()
	{
		return order_id;
	}
	public void setOrder_id(String orderId)
	{
		order_id = orderId;
	}
	public String getBillno()
	{
		return billno;
	}
	public void setBillno(String billno)
	{
		this.billno = billno;
	}
	public String getAccount()
	{
		return account;
	}
	public void setAccount(String account)
	{
		this.account = account;
	}
	public String getAmount()
	{
		return amount;
	}
	public void setAmount(String amount)
	{
		this.amount = amount;
	}
	public int getStatus()
	{
		return status;
	}
	public void setStatus(int status)
	{
		this.status = status;
	}
	public String getApp_id()
	{
		return app_id;
	}
	public void setApp_id(String appId)
	{
		app_id = appId;
	}
	public String getUuid()
	{
		return uuid;
	}
	public void setUuid(String uuid)
	{
		this.uuid = uuid;
	}
	public String getZone()
	{
		return zone;
	}
	public void setZone(String zone)
	{
		this.zone = zone;
	}
	public String getRoleid()
	{
		return roleid;
	}
	public void setRoleid(String roleid)
	{
		this.roleid = roleid;
	}
	
}
