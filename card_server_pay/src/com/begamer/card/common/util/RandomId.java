package com.begamer.card.common.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * 
 * @ClassName: RandomId
 * @Description:生成唯一的字符串
 * @author gs
 * @date 2014-3-13 下午04:29:48
 * 
 */
public class RandomId {
	private  Random random;
	private  String table;

	public RandomId() {
		random = new Random();
		table = "0123456789";
	}

	/**
	 * 
	 * @Title: randomId
	 * @Description:根据一个有序的int id, 生成10位唯一字符串(不足10位后面补1-9的随机数)
	 * @param id
	 * @return
	 * @author gs
	 * @date 2014-3-13 PM 03:34:43
	 */
	public  String randomId(int id) {
		String ret = null, num = String.format("%05d", id);
		int key = random.nextInt(10), seed = random.nextInt(100);
		Caesar caesar = new Caesar(table, seed);
		num = caesar.encode(key, num);
		ret = num + String.format("%01d", key) + String.format("%02d", seed);
		while (ret.length() < 10) {
			ret += ((int) (Math.random() * 9) + 1);
		}
		return ret;
	}

	public static void main(String[] args) {
		RandomId r = new RandomId();
		for (int i = 99999; i > 0; i--) {
			System.out.println(r.randomId(i));
		}
	}

	public static void main2(String[] args) {
		RandomId r = new RandomId();
		List<String> list = new ArrayList<String>();
		for (int i = 0; i < 10; i += 1) {
			list.add(r.randomId(i));
		}
		System.out.println("list.size:" + list.size());
		for (int i = 0; i < list.size(); i++) {
			if (i % 1000 == 0) {
				System.out.println("i:" + i);
			}
			String s = list.get(i);
			for (int j = 0; j < list.size(); j++) {
				if (j == i) {
					continue;
				} else {
					if (list.get(j).equals(s)) {
						System.out.println("find ths same one!");
						break;
					}
				}
			}
		}
		System.out.println("over");
	}
}