package com.begamer.card.model.dao;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.begamer.card.model.pojo.PayInfo;

public class UserDao extends HibernateDaoSupport {

	@SuppressWarnings("unchecked")
	public List<PayInfo> getAllByPage(final int currentPage, final int pageSize)
	{

		return this.getHibernateTemplate().executeFind(new HibernateCallback() {

			public Object doInHibernate(Session session)
					throws HibernateException, SQLException
			{

				Query query = session.createQuery("from PayInfo p");
				query.setMaxResults(pageSize);
				query.setFirstResult((currentPage - 1) * pageSize);
				return query.list();
			}

		});
	}

	@SuppressWarnings("unchecked")
	public List<PayInfo> getAll()
	{
		try
		{
			return getHibernateTemplate().find("from PayInfo p");
		}
		catch (RuntimeException e)
		{
			throw e;
		}
	}

}
