package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.begamer.card.common.util.StringUtil;

public class LevelGiftData implements PropertyReader
{
	public int id;
	public int level;
	public List<String> goods;
	
	private static HashMap<Integer, LevelGiftData> data =new HashMap<Integer, LevelGiftData>();
	private static HashMap<Integer, LevelGiftData> data2 =new HashMap<Integer, LevelGiftData>();
	@Override
	public void addData()
	{
		data.put(level, this);
		data2.put(id, this);
	}

	@Override
	public void parse(String[] ss)
	{
		int location=0;
		id =StringUtil.getInt(ss[location]);
		level =StringUtil.getInt(ss[location+1]);
		goods =new ArrayList<String>();
		for(int i=0;i<6;i++)
		{
			location =2+i*2;
			int type =StringUtil.getInt(ss[location]);
			String item =StringUtil.getString(ss[location+1]);
			if(type !=0)
			{
				String str =type+"-"+item;
				goods.add(str);
			}
		}
		addData();
	}

	@Override
	public void resetData()
	{
		data.clear();
	}
	
	/**根据等级获取data**/
	public static LevelGiftData getLevelGiftData(int index)
	{
		return data.get(index);
	}
	
	/**根据id获取data**/
	public static LevelGiftData getLevelGiftData2(int index)
	{
		return data2.get(index);
	}

}
