package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ImaginationdropData implements PropertyReader
{
	/**编号**/
	public int id;
	/**技能掉落类型**/
	public int droptypeID;
	/**技能id**/
	public int skillID;
	/**几率**/
	public int probability;
	
	private static HashMap<Integer, ImaginationdropData> data = new HashMap<Integer, ImaginationdropData>();
	@Override
	public void addData() {
		data.put(id, this);
	}

	@Override
	public void parse(String[] ss) {

	}

	@Override
	public void resetData() {
		data.clear();
	}
	/**根据编号获取一个imaginationDropData**/
	public static ImaginationdropData getImaginationdropData(int i)
	{
		return data.get(i);
	}

	/**根据掉落序列id筛选dataList**/
	public static List<ImaginationdropData> getRandomDropData(int dropID)
	{
		List<ImaginationdropData> list =new ArrayList<ImaginationdropData>();
		for(ImaginationdropData iData : data.values())
		{
			if(iData.droptypeID ==dropID)
			{
				list.add(iData);
			}
		}
		return list;
	}
}
