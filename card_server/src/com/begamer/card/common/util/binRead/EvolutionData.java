package com.begamer.card.common.util.binRead;

import java.util.HashMap;

public class EvolutionData implements PropertyReader
{
	//==编号==//
	public int id;
	//==星级==//
	public int stars;
	//==突破次数==//
	public int evo;
	//==需要卡数==//
	public int cards;
	//==上限提升==//
	public int lvl;
	//==属性提升==//
	public int status;
	//==消耗的金罡心==//
	public int moneypercard;

	private static HashMap<String, EvolutionData> data=new HashMap<String, EvolutionData>();
	
	@Override
	public void addData()
	{
		data.put(stars+"-"+evo,this);
	}

	@Override
	public void parse(String[] ss)
	{
	}

	@Override
	public void resetData()
	{
		data.clear();
	}

	public static EvolutionData getData(int star,int evo)
	{
		return data.get(star+"-"+evo);
	}
}
