package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.begamer.card.common.util.StringUtil;
import com.begamer.card.log.ErrorLogger;

public class GiftData implements PropertyReader
{
	public int id;
	public int time;//时间
	public List<String> reward;//奖励类型-奖励
	
	private static HashMap<Integer, GiftData> data =new HashMap<Integer, GiftData>();
	private static List<GiftData> dataList =new ArrayList<GiftData>();
	private static final Logger errorlogger=ErrorLogger.logger;
	@Override
	public void addData()
	{
		data.put(id, this);
		dataList.add(this);
	}

	@Override
	public void parse(String[] ss)
	{
		int location=0;
		id =StringUtil.getInt(ss[location]);
		time =StringUtil.getInt(ss[location+1]);
		location =2;
		reward =new ArrayList<String>();
		for(int i=0;i<3;i++)
		{
			location =2+i*2;
			int type =StringUtil.getInt(ss[location]);
			String ward =StringUtil.getString(ss[location+1]);
			if(type ==0)
			{
				continue;
			}
			else
			{
				try 
				{
					if(type<=5)
					{
						String [] temp =ward.split(",");
						int wardId =StringUtil.getInt(temp[0]);
						int num =StringUtil.getInt(temp[1]);
					}
					String restr =type+"-"+ward;
					reward.add(restr);
				}
				catch(Exception e)
				{
					errorlogger.error("加载gift奖励数据格式错误："+id,e);
					System.exit(0);
				}
				
			}
		}
		addData();
	}

	@Override
	public void resetData()
	{
		data.clear();
		dataList.clear();
	}

	/**根据id获取data**/
	public static GiftData getGiftData(int index)
	{
		return data.get(index);
	}
	
	public static List<GiftData> getDataList()
	{
		return dataList;
	}
}
