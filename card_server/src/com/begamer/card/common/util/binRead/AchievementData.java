package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.begamer.card.common.util.StringUtil;
import com.begamer.card.log.ErrorLogger;

public class AchievementData implements PropertyReader
{
	/**编号**/
	public int id;
	/**前置成就**/
	public int exachieve;
	/**成就名称**/
	public String name;
	/**成就类型**/
	public int type;
	/**成就要求**/
	public String request;
	/**描述**/
	public String description;
	/**成就图标**/
	public String icon;
	/**奖励类别-奖励内容**/
	public List<String> reward;
	public int disable;

	private static HashMap<Integer, AchievementData> data=new HashMap<Integer, AchievementData>();
	private static List<AchievementData> dataList=new ArrayList<AchievementData>();
	private static final Logger errorlogger=ErrorLogger.logger;
	
	@Override
	public void addData()
	{
		data.put(id, this);
		dataList.add(this);
	}

	@Override
	public void parse(String[] ss)
	{
		int location=0;
		id=StringUtil.getInt(ss[location++]);
		exachieve=StringUtil.getInt(ss[location++]);
		name=StringUtil.getString(ss[location++]);
		type=StringUtil.getInt(ss[location++]);
		request=StringUtil.getString(ss[location++]);
		description=StringUtil.getString(ss[location++]);
		icon=StringUtil.getString(ss[location++]);
		
		int length=(ss.length-location)/2;
		reward=new ArrayList<String>();
		for(int i=0;i<length;i++)
		{
			int rewardType=StringUtil.getInt(ss[location++]);
			String rewardDetail=StringUtil.getString(ss[location++]);
			try
			{
				if(rewardType!=0)
				{
					if(rewardType<=5)
					{
						String [] temp =rewardDetail.split(",");
						int rewardId =StringUtil.getInt(temp[0]);
						int num =StringUtil.getInt(temp[1]);
					}
					reward.add(rewardType+"-"+rewardDetail);
				}
			} 
			catch (Exception e) 
			{
				errorlogger.error("加载achievement表奖励物品格式错误:"+id,e);
				System.exit(0);
			}
		}
		disable =StringUtil.getInt(ss[location++]);
		addData();
	}

	@Override
	public void resetData()
	{
		data.clear();
		dataList.clear();
	}

	public static AchievementData getData(int id)
	{
		return data.get(id);
	}
	
	public static List<AchievementData> getNextAchieveMents(int id)
	{
		List<AchievementData> result=new ArrayList<AchievementData>();
		for(AchievementData ad:data.values())
		{
			if(ad.exachieve==id)
			{
				result.add(ad);
			}
		}
		return result;
	}
	
	/**根据type获取data**/
	public static List<AchievementData> getAchievementByType(int type)
	{
		List<AchievementData> ads =new ArrayList<AchievementData>();
		for(AchievementData ad:data.values())
		{
			if(ad.type==type)
			{
				ads.add(ad);
			}
		}
		return ads;
	}
}
