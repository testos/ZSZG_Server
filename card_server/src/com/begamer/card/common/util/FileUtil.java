package com.begamer.card.common.util;

import java.io.File;

import org.apache.log4j.Logger;

/**
 * 
 * @ClassName: FileUtil
 * @Description: TODO
 * @author gs
 * @date Nov 10, 2011 3:50:06 PM
 * 
 */
public class FileUtil {

	private static final Logger logger = Logger.getLogger(FileUtil.class);

	/**
	 */
	public static boolean delFile(File file) {
		try {
			if (file.exists()) {
				boolean flag = file.delete();
				String[] arys = file.getParentFile().list();
				if (null == arys || arys.length == 0) {
					return delFile(file.getParentFile());
				}
				return flag;
			} else {
				return false;
			}
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}
}
