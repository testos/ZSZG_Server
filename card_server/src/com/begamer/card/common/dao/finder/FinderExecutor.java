package com.begamer.card.common.dao.finder;

import java.lang.reflect.Method;
import java.util.List;

//import org.hibernate.ScrollableResults;

public interface FinderExecutor<T> {
	List<T> executeFinder(Method method, Object[] queryArgs);
	// Iterator<T> iteratorFinder(Method method, Object[] queryArgs);

	// ScrollableResults scrollFinder(Method method, Object[] queryArgs);
}
