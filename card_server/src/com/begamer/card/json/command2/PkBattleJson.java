package com.begamer.card.json.command2;

import com.begamer.card.json.BasicJson;

public class PkBattleJson extends BasicJson
{
	/**pk对象id**/
	public int pkId;

	public int getPkId() {
		return pkId;
	}

	public void setPkId(int pkId) {
		this.pkId = pkId;
	}
}
