package com.begamer.card.json.command2;

import java.util.List;

import com.begamer.card.json.ErrorJson;
import com.begamer.card.json.element.KOawardElement;

public class KOExchangeResultJson extends ErrorJson
{
	public int point;//当前玩家积分
	public List<KOawardElement> awards;//奖励
	public int getPoint() {
		return point;
	}

	public void setPoint(int point) {
		this.point = point;
	}

	public List<KOawardElement> getAwards() {
		return awards;
	}

	public void setAwards(List<KOawardElement> awards) {
		this.awards = awards;
	}
}
