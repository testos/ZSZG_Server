package com.begamer.card.json.command2;

import com.begamer.card.json.BasicJson;

public class ActivityGJson extends BasicJson{

	public int id;//活动id
	public int type;//类型
	
	public int getId()
	{
		return id;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	public int getType()
	{
		return type;
	}
	public void setType(int type)
	{
		this.type = type;
	}
}
