package com.begamer.card.json.command2;

import com.begamer.card.json.ErrorJson;

public class MazeClearResultJson extends ErrorJson
{
	public String maze;//迷宫id-位置-次数-付费次数
	public int crystal;//水晶数
	
	public String getMaze() {
		return maze;
	}
	public void setMaze(String maze) {
		this.maze = maze;
	}
	public int getCrystal() {
		return crystal;
	}
	public void setCrystal(int crystal) {
		this.crystal = crystal;
	}
	
	
}
