package com.begamer.card.json.command;

import java.util.List;

import com.begamer.card.json.ErrorJson;
import com.begamer.card.model.pojo.Player;

public class RefreshHelpPlayerResultJson extends ErrorJson
{
	public List<Player> helpPlayers;
	public int crystal;
	
	public List<Player> getHelpPlayers()
	{
		return helpPlayers;
	}

	public void setHelpPlayers(List<Player> helpPlayers)
	{
		this.helpPlayers = helpPlayers;
	}

	public int getCrystal()
	{
		return crystal;
	}

	public void setCrystal(int crystal)
	{
		this.crystal = crystal;
	}

}
