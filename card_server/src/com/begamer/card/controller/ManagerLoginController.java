package com.begamer.card.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;

import com.begamer.card.cache.Cache;
import com.begamer.card.common.Constant;
import com.begamer.card.common.util.RequestUtil;
import com.begamer.card.common.util.StringUtil;
import com.begamer.card.log.ManagerLogger;
import com.begamer.card.model.dao.ManagerLoginDao;
import com.begamer.card.model.pojo.Manager;

/** GM管理员控制器 */
public class ManagerLoginController extends AbstractMultiActionController {
	
	@Autowired
	private ManagerLoginDao managerLoginDao;
	
	private static Logger logger = Logger.getLogger(ManagerLoginController.class);
	private static Logger managerLogger = ManagerLogger.logger;
	
	/** 去登陆 */
	public ModelAndView index(HttpServletRequest request, HttpServletResponse response)
	{		
		return new ModelAndView("/gm/Login.vm");
	}
	
	/** 登陆 */
	public ModelAndView Login(HttpServletRequest request, HttpServletResponse response)
	{
		
		try
		{
			// 获取数据
			String name = request.getParameter("name");
			String password = request.getParameter("password");
			String rand = RequestUtil.GetParamString(request, "rand", null);
			Manager manager = managerLoginDao.ManagerLogin(name, password);
			
			if (null != request.getSession().getAttribute(Constant.IMAGE_SESSION) && rand != null && rand.equals((String) request.getSession().getAttribute(Constant.IMAGE_SESSION)))
			{	

			}
			else
			{
				if (name != null || password != null || rand != null)
				{
					request.setAttribute("showmsg", "验证码错误,请重试！");
				}
				return index(request, response);
			}
			
			if (manager != null)
			{
				request.getSession().setAttribute(Constant.LOGIN_USER, manager);
				request.setAttribute("manager", manager);
				logger.info("用户" + manager.getName() + "登录成功！" + "登录IP" + request.getRemoteAddr());
				managerLogger.info(manager.getName() + "|用户" + manager.getName() + "登录成功！" + "|登录IP" + request.getRemoteAddr());
				return new ModelAndView("/gm/index.vm");
			}
			else
			{
				request.setAttribute("showmsg", "用户名密码错误,请重试！");
				return new ModelAndView("/gm/Login.vm");
			}
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
	/***************************************************************************
	 * 注册
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	public ModelAndView addManager(HttpServletRequest request, HttpServletResponse response)
	{
		
		String name = request.getParameter("name");
		String password = request.getParameter("password");
		Manager manager = new Manager();
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String day = sdf.format(date);
		manager.setCreateOn(day);
		manager.setName(name);
		manager.setPassword(password);
		Manager manager2 = managerLoginDao.getManagerByName(name);
		if (manager2 != null)
		{
			request.setAttribute("show", "此用户已经注册！请重新录入信息！");
			return new ModelAndView("/gm/addManager.vm");
		}
		else
		{
			managerLoginDao.addManager(manager);
			return new ModelAndView("/gm/succeed.vm");
		}
	}
	
	/***************************************************************************
	 * 添加用户
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	public ModelAndView addManager2(HttpServletRequest request, HttpServletResponse response)
	{
		String name = request.getParameter("name");
		String password = request.getParameter("password");
		int serverId = Integer.parseInt(request.getParameter("serverId"));
		Manager manager = new Manager();
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String day = sdf.format(date);
		manager.setCreateOn(day);
		manager.setName(name);
		manager.setPassword(password);
		manager.setServerId(serverId);
		Manager manager2 = managerLoginDao.getManagerByName(name);
		if (manager2 != null)
		{
			request.setAttribute("show", "此用户已经注册！请重新录入信息！");
			return new ModelAndView("/gm/addManager2.vm");
		}
		else
		{
			managerLoginDao.addManager(manager);
			getManager(request, response);
			return new ModelAndView("/gm/managerAll.vm");
		}
	}
	
	/***************************************************************************
	 * 全查询
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	public ModelAndView getManager(HttpServletRequest request, HttpServletResponse response)
	{
		boolean is = checkQx(getManager(request).getId());
		if (!is)
		{
			return new ModelAndView("gm/noqx.vm");
		}
		Manager manager = (Manager) request.getSession().getAttribute(Constant.LOGIN_USER);
		request.setAttribute("user", manager);
		List<Manager> managers = managerLoginDao.getManagers();
		request.setAttribute("managers", managers);
		return new ModelAndView("/gm/managerAll.vm");
	}
	
	// 修改
	public ModelAndView upManager(HttpServletRequest request, HttpServletResponse response)
	{
		
		int id = Integer.parseInt(request.getParameter("id"));
		String name = request.getParameter("name");
		String password = request.getParameter("password");
		Manager manager = new Manager();
		manager.setName(name);
		manager.setPassword(password);
		manager.setId(id);
		managerLoginDao.upManager(manager);
		getManager(request, response);
		return new ModelAndView("/gm/managerAll.vm");
	}
	
	public ModelAndView one(HttpServletRequest request, HttpServletResponse response)
	{
		int id = Integer.parseInt(request.getParameter("id"));
		Manager manager = managerLoginDao.getOneById(id);
		request.setAttribute("manager", manager);
		return new ModelAndView("/gm/upManager.vm");
	}
	
	/** 去注册 **/
	public ModelAndView go(HttpServletRequest request, HttpServletResponse response)
	{
		return new ModelAndView("/gm/addManager.vm");
	}
	
	/** 去添加 **/
	public ModelAndView goAdd(HttpServletRequest request, HttpServletResponse response)
	{
		return new ModelAndView("/gm/addManager.vm");
	}
	
	/**
	 * 
	 * @Title: logout
	 * @Description: TODO 登出
	 */
	public ModelAndView logout(HttpServletRequest request, HttpServletResponse response)
	{
		
		try
		{
			request.getSession().removeAttribute(Constant.LOGIN_USER);
			return new ModelAndView("/gm/login.vm");
		}
		catch (Exception e)
		{
			logger.error(e.toString());
			return new ModelAndView("error.vm");
		}
	}
	
	public ModelAndView main(HttpServletRequest request, HttpServletResponse response)
	{
		
		return new ModelAndView("/gm/main.vm");
	}
	
	public ModelAndView top(HttpServletRequest request, HttpServletResponse response)
	{
		Manager manager = (Manager) request.getSession().getAttribute(Constant.LOGIN_USER);
		request.setAttribute("server", Cache.getInstance().serverName);
		request.setAttribute("manager", manager);
		return new ModelAndView("/gm/top.vm");
	}
	
	public ModelAndView menu(HttpServletRequest request, HttpServletResponse response)
	{
		return new ModelAndView("/gm/menu.vm");
	}
	
	public ModelAndView left(HttpServletRequest request, HttpServletResponse response)
	{
		return new ModelAndView("/gm/left.vm");
	}
	
	public ModelAndView archives(HttpServletRequest request, HttpServletResponse response)
	{
		return new ModelAndView("/gm/archives.vm");
	}
	
	/** **根据名称查询Manager(检查是否注册)*** */
	public ModelAndView getManagerByName(HttpServletRequest request, HttpServletResponse response)
	{
		
		response.setCharacterEncoding("UTF-8");
		String name = request.getParameter("name");
		
		try
		{
			Manager manager = managerLoginDao.getManagerByName(name);
			PrintWriter out = response.getWriter();
			
			if (manager != null)
			{
				String s = "此用户已经注册！";
				out.print(s);
				out.close();
			}
			else
			{
				String s = "恭喜，此用户可以注册！";
				out.print(s);
				out.close();
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		
		return null;
	}
	
	// 模糊GM名称查询
	public ModelAndView mhg_name(HttpServletRequest request, HttpServletResponse response)
	{
		String name = request.getParameter("g_name");
		if (name.trim().length() > 0 && name != null)
		{
			List<Manager> managers = managerLoginDao.mh_name(name);
			if (managers.size() > 0 && managers != null)
			{
				request.setAttribute("managers", managers);
			}
			else
			{
				request.setAttribute("shown", "没有该用户");
			}
		}
		else
		{
			getManager(request, response);
		}
		return new ModelAndView("/gm/oneManager.vm");
	}
	
	/** **根据ID查询*** */
	public ModelAndView mhg_id(HttpServletRequest request, HttpServletResponse response)
	{
		String g_id = request.getParameter("g_id");
		Manager manager = managerLoginDao.getOneById(Integer.parseInt(g_id));
		if (manager != null)
		{
			request.setAttribute("manager", manager);
		}
		else
		{
			request.setAttribute("showi", "没有该用户");
		}
		return new ModelAndView("/gm/oneManager.vm");
	}
	
	/** ****去首页**** */
	public ModelAndView goMain(HttpServletRequest request, HttpServletResponse response)
	{
		return new ModelAndView("/gm/index.vm");
	}
	/**删除**/
	public ModelAndView del(HttpServletRequest request,HttpServletResponse response)
	{
		int id = StringUtil.getInt(request.getParameter("id"));
		Manager manager = new Manager();
		manager.setId(id);
		managerLoginDao.delManager(manager);
		return getManager(request,response);
	}
}
