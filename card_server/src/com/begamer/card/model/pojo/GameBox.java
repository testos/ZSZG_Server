package com.begamer.card.model.pojo;

public class GameBox
{
	public int id;
	public int gamebox_id;
	public int gamebox_num;
	public int gamebox_time;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getGamebox_id() {
		return gamebox_id;
	}
	public void setGamebox_id(int gameboxId) {
		gamebox_id = gameboxId;
	}
	public int getGamebox_num() {
		return gamebox_num;
	}
	public void setGamebox_num(int gameboxNum) {
		gamebox_num = gameboxNum;
	}
	public int getGamebox_time() {
		return gamebox_time;
	}
	public void setGamebox_time(int gameboxTime) {
		gamebox_time = gameboxTime;
	}
	
}
