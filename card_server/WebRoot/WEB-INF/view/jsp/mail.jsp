<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>发送邮件</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<script type="text/javascript">
	  	function check()
	  	{
	  		if(document.getElementById("title").value=="" || document.getElementById("title").value==null)
	  		{
	  			alert("请填写邮件标题");
	  			document.getElementById("title").focus();
	  			return;
	  		}
	  		if(document.getElementById("deleteMinute").value=="" || document.getElementById("deleteMinute").value==null)
	  		{
	  			alert("保留时间最小单位分不能为空");
	  			document.getElementById("deleteMinute").focus();
	  			return;
	  		}
	  		var reg=/[\D\s]/;
	  		var ele=document.getElementById("deleteDay");
	  		if(ele.value!=null && ele.value!="" && reg.exec(ele.value))
	  		{
	  			alert("天数只能填整数");
	  			ele.focus();
	  			return;
	  		}
	  		ele=document.getElementById("deleteHour");
	  		if(ele.value!=null && ele.value!="" && reg.exec(ele.value))
	  		{
	  			alert("小时只能填整数");
	  			ele.focus();
	  			return;
	  		}
	  		ele=document.getElementById("deleteMinute");
	  		if(ele.value!=null && ele.value!="" && reg.exec(ele.value))
	  		{
	  			alert("分钟只能填整数");
	  			ele.focus();
	  			return;
	  		}
	  		ele=document.getElementById("gold");
	  		if(ele.value!=null && ele.value!="" && reg.exec(ele.value))
	  		{
	  			alert("金币只能填整数");
	  			ele.focus();
	  			return;
	  		}
	  		ele=document.getElementById("crystal");
	  		if(ele.value!=null && ele.value!="" && reg.exec(ele.value))
	  		{
	  			alert("钻石只能填整数");
	  			ele.focus();
	  			return;
	  		}
	  		ele=document.getElementById("runeNum");
	  		if(ele.value!=null && ele.value!="" && reg.exec(ele.value))
	  		{
	  			alert("符文值只能填整数");
	  			ele.focus();
	  			return;
	  		}
	  		ele=document.getElementById("power");
	  		if(ele.value!=null && ele.value!="" && reg.exec(ele.value))
	  		{
	  			alert("体力只能填整数");
	  			ele.focus();
	  			return;
	  		}
	  		ele=document.getElementById("friendNum");
	  		if(ele.value!=null && ele.value!="" && reg.exec(ele.value))
	  		{
	  			alert("友情值只能填整数");
	  			ele.focus();
	  			return;
	  		}
	  		ele=document.getElementById("rewardId1");
	  		if(ele.value!=null && ele.value!="" && reg.exec(ele.value))
	  		{
	  			alert("附带物品1的物品Id只能填整数");
	  			ele.focus();
	  			return;
	  		}
	  		ele=document.getElementById("rewardNumber1");
	  		if(ele.value!=null && ele.value!="" && reg.exec(ele.value))
	  		{
	  			alert("附带物品1的物品个数只能填整数");
	  			ele.focus();
	  			return;
	  		}
	  		ele=document.getElementById("rewardId2");
	  		if(ele.value!=null && ele.value!="" && reg.exec(ele.value))
	  		{
	  			alert("附带物品2的物品Id只能填整数");
	  			ele.focus();
	  			return;
	  		}
	  		ele=document.getElementById("rewardNumber2");
	  		if(ele.value!=null && ele.value!="" && reg.exec(ele.value))
	  		{
	  			alert("附带物品2的物品个数只能填整数");
	  			ele.focus();
	  			return;
	  		}
	  		ele=document.getElementById("rewardId3");
	  		if(ele.value!=null && ele.value!="" && reg.exec(ele.value))
	  		{
	  			alert("附带物品3的物品Id只能填整数");
	  			ele.focus();
	  			return;
	  		}
	  		ele=document.getElementById("rewardNumber3");
	  		if(ele.value!=null && ele.value!="" && reg.exec(ele.value))
	  		{
	  			alert("附带物品3的物品个数只能填整数");
	  			ele.focus();
	  			return;
	  		}
	  		ele=document.getElementById("rewardId4");
	  		if(ele.value!=null && ele.value!="" && reg.exec(ele.value))
	  		{
	  			alert("附带物品4的物品Id只能填整数");
	  			ele.focus();
	  			return;
	  		}
	  		ele=document.getElementById("rewardNumber4");
	  		if(ele.value!=null && ele.value!="" && reg.exec(ele.value))
	  		{
	  			alert("附带物品4的物品个数只能填整数");
	  			ele.focus();
	  			return;
	  		}
	  		ele=document.getElementById("rewardId5");
	  		if(ele.value!=null && ele.value!="" && reg.exec(ele.value))
	  		{
	  			alert("附带物品5的物品Id只能填整数");
	  			ele.focus();
	  			return;
	  		}
	  		ele=document.getElementById("rewardNumber5");
	  		if(ele.value!=null && ele.value!="" && reg.exec(ele.value))
	  		{
	  			alert("附带物品5的物品个数只能填整数");
	  			ele.focus();
	  			return;
	  		}
	  		ele=document.getElementById("rewardId6");
	  		if(ele.value!=null && ele.value!="" && reg.exec(ele.value))
	  		{
	  			alert("附带物品6的物品Id只能填整数");
	  			ele.focus();
	  			return;
	  		}
	  		ele=document.getElementById("rewardNumber6");
	  		if(ele.value!=null && ele.value!="" && reg.exec(ele.value))
	  		{
	  			alert("附带物品6的物品个数只能填整数");
	  			ele.focus();
	  			return;
	  		}
	  		//批量发送玩家id不能为空
	  		var sel_obj=document.getElementById("type");
			var index=sel_obj.selectedIndex;
	  		var temp=sel_obj.options[index].value;
	  		ele=document.getElementById("receiver");
	  		if(temp==2 && (ele.value==null || ele.value==""))
	  		{
	  			alert("批量发送时必须写明玩家Id");
	  			ele.focus();
	  			return;
	  		}
	  		
	  		document.getElementById("btn").disabled="disabled";
	  		document.form1.submit();
	  	}
	  	
	  	function showTips()
	  	{
			var sel_obj=document.getElementById("type");
			var index=sel_obj.selectedIndex;
	  		var temp=sel_obj.options[index].value;
	  		if(temp==1)
	  		{
	  			document.getElementById("tip").style["display"]="none";
	  		}
	  		else if(temp==2)
	  		{
	  			document.getElementById("tip").style["display"]="block";
	  		}
	  	}
	  	
	  	function changeRewardType1()
	  	{
	  		document.getElementById("rewardId1").value="";
	  		document.getElementById("rewardNumber1").value="";
	  	}
	  	function changeRewardType2()
	  	{
	  		document.getElementById("rewardId2").value="";
	  		document.getElementById("rewardNumber2").value="";
	  	}
	  	function changeRewardType3()
	  	{
	  		document.getElementById("rewardId3").value="";
	  		document.getElementById("rewardNumber3").value="";
	  	}
 	</script>
 	<script type="text/javascript" src="<%=path%>/resource/js/jquery_validate/lib/jquery-1.4.2.min.js"></script>
 	<script type="text/javascript" src="<%=path%>/resource/js/jquery_validate/lib/WdatePicker.js"></script>
	<script type="text/javascript" src="<%=path%>/resource/js/mail.js"></script>
  </head>
  <body>
	<form action="gm.htm?action=sendMail" method="post" name="form1">
		<table align="center" border="0" cellpadding="1" cellspacing="0">
			<tr>
				<td colspan="2" align="center">
					<font size="24">邮件发送</font>
					<% 
				  		String msg=(String)request.getAttribute("msg");
				  		if("success".equals(msg))
				  		{
				  			out.println("<font color=red>(发送成功)</font>");
				  		}
				  		else if("fail".equals(msg))
				  		{
				  			out.println("<font color=red>(发送失败)</font>");
				  		}
				  	 %>
				</td>
			</tr>
			<tr>
				<td>发放方式</td>
				<td>
					<select name="type" id="type" onchange="javascript:showTips();">
						<option value="1" selected="selected">全服发放</option>
						<option value="2">批量发放</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>邮件标题</td>
				<td><input type="text" name="title" id="title" value="" maxlength="20" size="44"><font color="red">*必填</font></td>
			</tr>
			<tr>
				<td>保存时间</td>
				<td>
					<input type="text" name="deleteDay" id="deleteDay" value="" maxlength="2">天
					<input type="text" name="deleteHour" id="deleteHour" value="" maxlength="2">时
					<input type="text" name="deleteMinute" id="deleteMinute" value="" maxlength="2">分<font color="red">*只能填整数</font>
				</td>
			</tr>
			<tr id="tip" style="display: none;">
				<td>玩家id</td>
				<td><input type="text" name="receiver" id="receiver" value="" maxlength="2000" size="80"><font color="red">*多个玩家id之间用英文","号分割</font></td>
			</tr>
			<tr>
				<td>金币</td>
				<td><input type="text" name="gold" id="gold" value="" maxlength="8"><font color="red">*只能填整数,且小于1亿</font></td>
			</tr>
			<tr>
				<td>钻石</td>
				<td><input type="text" name="crystal" id="crystal" value="" maxlength="8"><font color="red">*只能填整数,且小于1亿</font></td>
			</tr>
			<tr>
				<td>符文值</td>
				<td><input type="text" name="runeNum" id="runeNum" value="" maxlength="8"><font color="red">*只能填整数,且小于1亿</font></td>
			</tr>
			<tr>
				<td>体力</td>
				<td><input type="text" name="power" id="power" value="" maxlength="8"><font color="red">*只能填整数,且小于1亿</font></td>
			</tr>
			<tr>
				<td>友情值</td>
				<td><input type="text" name="friendNum" id="friendNum" value="" maxlength="8"><font color="red">*只能填整数,且小于1亿</font></td>
			</tr>
			<tr>
				<td>邮件内容</td>
				<td><textarea name="content" id="content" rows="4" cols="60" onkeyup="if(this.value.length>1000) this.value=this.value.substr(0,1000)" onkeydown="if(this.value.length>1000) this.value=this.value.substr(0,1000)"></textarea><font color="red">*最多1000个字符</font></td>
			</tr>
			<tr>
				<td>附带物品1</td>
				<td>
					<select name="rewardType1" id="rewardType1">
						<option value="0">--物品类型--</option>
						<option value="1">材料</option>
						<option value="2">装备</option>
						<option value="3">英雄卡</option>
						<option value="4">主动技能</option>
						<option value="5">被动技能</option>
					</select>
					物品Id:<!--<input type="text" name="rewardId1" id="rewardId1" value="" maxlength="8" size="10">-->
					<select name="rewardId1" id="rewardId1" style="width:160px;">
						<option value="0">-----------物品名称-----------</option>
					</select>
					物品个数:<input type="text" name="rewardNumber1" id="rewardNumber1" value="" maxlength="4" size="10"><font color="red">*只能填整数,且小于1万</font>
				</td>
			</tr>
			<tr>
				<td>附带物品2</td>
				<td>
					<select name="rewardType2" id="rewardType2">
						<option value="0">--物品类型--</option>
						<option value="1">材料</option>
						<option value="2">装备</option>
						<option value="3">英雄卡</option>
						<option value="4">主动技能</option>
						<option value="5">被动技能</option>
					</select>
					物品Id:<!-- <input type="text" name="rewardId2" id="rewardId2" value="" maxlength="8" size="10"> -->
					<select name="rewardId2" id="rewardId2" style="width:160px;">
						<option value="0">-----------物品名称-----------</option>
					</select>
					物品个数:<input type="text" name="rewardNumber2" id="rewardNumber2" value="" maxlength="4" size="10"><font color="red">*只能填整数,且小于1万</font>
				</td>
			</tr>
			<tr>
				<td>附带物品3</td>
				<td>
					<select name="rewardType3" id="rewardType3">
						<option value="0">--物品类型--</option>
						<option value="1">材料</option>
						<option value="2">装备</option>
						<option value="3">英雄卡</option>
						<option value="4">主动技能</option>
						<option value="5">被动技能</option>
					</select>
					物品Id:<!-- <input type="text" name="rewardId3" id="rewardId3" value="" maxlength="8" size="10">  -->
					<select name="rewardId3" id="rewardId3" style="width:160px;">
						<option value="0">-----------物品名称-----------</option>
					</select>
					物品个数:<input type="text" name="rewardNumber3" id="rewardNumber3" value="" maxlength="4" size="10"><font color="red">*只能填整数,且小于1万</font>
				</td>
			</tr>
			<tr>
				<td>附带物品4</td>
				<td>
					<select name="rewardType4" id="rewardType4">
						<option value="0">--物品类型--</option>
						<option value="1">材料</option>
						<option value="2">装备</option>
						<option value="3">英雄卡</option>
						<option value="4">主动技能</option>
						<option value="5">被动技能</option>
					</select>
					物品Id:<!-- <input type="text" name="rewardId3" id="rewardId3" value="" maxlength="8" size="10">  -->
					<select name="rewardId4" id="rewardId4" style="width:160px;">
						<option value="0">-----------物品名称-----------</option>
					</select>
					物品个数:<input type="text" name="rewardNumber4" id="rewardNumber4" value="" maxlength="4" size="10"><font color="red">*只能填整数,且小于1万</font>
				</td>
			</tr>
			<tr>
				<td>附带物品5</td>
				<td>
					<select name="rewardType5" id="rewardType5">
						<option value="0">--物品类型--</option>
						<option value="1">材料</option>
						<option value="2">装备</option>
						<option value="3">英雄卡</option>
						<option value="4">主动技能</option>
						<option value="5">被动技能</option>
					</select>
					物品Id:<!-- <input type="text" name="rewardId3" id="rewardId3" value="" maxlength="8" size="10">  -->
					<select name="rewardId5" id="rewardId5" style="width:160px;">
						<option value="0">-----------物品名称-----------</option>
					</select>
					物品个数:<input type="text" name="rewardNumber5" id="rewardNumber5" value="" maxlength="4" size="10"><font color="red">*只能填整数,且小于1万</font>
				</td>
			</tr>
			<tr>
				<td>附带物品6</td>
				<td>
					<select name="rewardType6" id="rewardType6">
						<option value="0">--物品类型--</option>
						<option value="1">材料</option>
						<option value="2">装备</option>
						<option value="3">英雄卡</option>
						<option value="4">主动技能</option>
						<option value="5">被动技能</option>
					</select>
					物品Id:<!-- <input type="text" name="rewardId3" id="rewardId3" value="" maxlength="8" size="10">  -->
					<select name="rewardId6" id="rewardId6" style="width:160px;">
						<option value="0">-----------物品名称-----------</option>
					</select>
					物品个数:<input type="text" name="rewardNumber6" id="rewardNumber6" value="" maxlength="4" size="10"><font color="red">*只能填整数,且小于1万</font>
				</td>
			</tr>
			<tr align="center">
				<td colspan="2"><input type="button" id="btn" value="点击发送" onclick="javascript:check();"></td>
			</tr>
		</table>
	</form>
  </body>
</html>
